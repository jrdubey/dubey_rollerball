﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreaController : MonoBehaviour
{
    public float speed = 100.0f;

    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
            transform.Rotate(Vector3.forward * speed * Time.deltaTime);

        if (Input.GetKey(KeyCode.D))
            transform.Rotate(-Vector3.forward * speed * Time.deltaTime);

        if (Input.GetKey(KeyCode.W))
            transform.Rotate(-Vector3.left * speed * Time.deltaTime);

        if (Input.GetKey(KeyCode.S))
            transform.Rotate(Vector3.left * speed * Time.deltaTime);

    }
}
